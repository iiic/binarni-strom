<?php //netteCache[01]000362a:2:{s:4:"time";s:21:"0.66894700 1377031566";s:9:"callbacks";a:2:{i:0;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:9:"checkFile";}i:1;s:40:"C:\xampp\htdocs\binarni-strom\readme.txt";i:2;i:1377030014;}i:1;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:10:"checkConst";}i:1;s:25:"Nette\Framework::REVISION";i:2;s:30:"80a7e46 released on 2013-08-08";}}}?><?php

// source file: C:\xampp\htdocs\binarni-strom\readme.txt

?><?php
// prolog Nette\Latte\Macros\CoreMacros
list($_l, $_g) = Nette\Latte\Macros\CoreMacros::initRuntime($template, 'byv3xetrdv')
;
// prolog Nette\Latte\Macros\UIMacros

// snippets support
if (!empty($_control->snippetMode)) {
	return Nette\Latte\Macros\UIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
?>
Binární strom
-------------

Jednoduchá treeview komponenta s funkcemi přidávání, editace mazání (smaže i
všechny potomky přes databázovou relaci).

Zadání a export databáze jsou v adresáři /external

Postavené na nette (stabilní verzi z 19.8.2013) sandboxu. Žádný další externí
kód není použit.

Výběr dat na výpis se provádí jediným dotazem, který načte všechna data z jediné
tabulky a o formátování na výpis už obstará php (pomocí rekurze). Načtená data
jsou uložena v private proměnné all. Odtud si data vezme například formulář
pro vložení/editaci, který je potřebuje pro zvolení rodiče vkládaného záznamu.

Nastavení je funkční na produkčním serveru

Veškeré kódování je UTF-8, konce řádků UNIX, pro odsazení použity tabulátory.

Všechny funkce jsou dostupné i s vypnutým javascriptem!!!

Testováno v aktuálních verzích prohlížečů FF, Opera, Chrome, IE a Maxthon.
Všude se chová stejně.
Generuje validní HTML5 kód



Postup
------

Vytvořil jsem binární strom jako běžnou stránku (model, presenter, view),
následně jsem z toho vytvořil komponentu, aby bylo možné vypsání binárního
stromu přes { control treeView } . Samotná komponenta je pak jen prázdná obálka,
která použije metody třídy TreeviewPresenter a šablonu treeView.latte .




Databáze
--------

v app/config/config.neon nastaveno:
jméno databáze: ukol
server: localhost
uživatel: root
heslo:



Známé bugy
----------

- není ošetřena možnost úplně prázdné databáze



Možnosti rozšíření
------------------

- přesun položek jejich přetažením
- testy
