Binární strom
-------------

Jednoduchá treeview komponenta s funkcemi přidávání, editace mazání (smaže i
všechny potomky přes databázovou relaci).

Zadání a export databáze jsou v adresáři /external

Postavené na nette (stabilní verzi z 19.8.2013) sandboxu. Žádný další externí
kód není použit.

Výběr dat na výpis se provádí jediným dotazem, který načte všechna data z jediné
tabulky a o formátování na výpis už obstará php (pomocí rekurze). Načtená data
jsou uložena v private proměnné all. Odtud si data vezme například formulář
pro vložení/editaci, který je potřebuje pro zvolení rodiče vkládaného záznamu.

Nastavení je funkční na produkčním serveru

Veškeré kódování je UTF-8, konce řádků UNIX, pro odsazení použity tabulátory.

Všechny funkce jsou dostupné i s vypnutým javascriptem!!!

Testováno v aktuálních verzích prohlížečů FF, Opera, Chrome, IE a Maxthon.
Všude se chová stejně.
Generuje validní HTML5 kód



Postup
------

Vytvořil jsem binární strom jako běžnou stránku (model, presenter, view),
následně jsem z toho vytvořil komponentu, aby bylo možné vypsání binárního
stromu přes { control treeView } . Samotná komponenta je pak jen prázdná obálka,
která použije metody třídy TreeviewPresenter a šablonu treeView.latte .




Databáze
--------

v app/config/config.neon nastaveno:
jméno databáze: ukol
server: localhost
uživatel: root
heslo:



Známé bugy
----------

- není ošetřena možnost úplně prázdné databáze



Možnosti rozšíření
------------------

- přesun položek jejich přetažením
- testy
