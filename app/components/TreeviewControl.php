<?php

use Nette\Application\UI;

class TreeviewControl extends UI\Control
{

	public function __construct()
	{
		parent::__construct();
	}



/*
	public function render()
	{
		$data = new NewsPresenter();
		$data->renderDefault();
		$this->template->items = $data->template->items;
		$this->template->setFile(APP_DIR.'/templates/News/default.latte');
		$this->template->render();
	}
*/

	public function render()
	{
		$data = new TreeviewPresenter($this->presenter->context);
		$data->renderDefault();

		$template = $this->template;
		$template->setFile(__DIR__ . '/../templates/Treeview/treeView.latte');
		$template->parents = $data->template->parents;
		$template->children = $data->template->children;
		$template->render();
	}

}
