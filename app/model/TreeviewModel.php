<?php

class TreeviewModel extends BaseModel
{

	private $db;



	function __construct(\Nette\Database\Connection $connection)
	{
		$this->db = $connection;
	}



	public function getAll() {
		return $this->db->fetchAll('SELECT *
			FROM '.BaseModel::TABLE.'
			ORDER BY name');
	}



	public function get($id) {
		return $this->db->fetch('SELECT parent_id, name, notice FROM '.BaseModel::TABLE.' WHERE id=?;', $id);
	}



	public function insert($values) {
		$this->db->table(BaseModel::TABLE)->insert($values);
	}



	public function update($id, $values) {
		$this->db->table(BaseModel::TABLE)
			->where('id', $id)
			->update($values);
	}



	public function delete($id) {
		$this->db->table(BaseModel::TABLE)->where('id', $id)->delete();
	}



	public function getName($id) {
		return $this->db->fetchColumn('SELECT name FROM '.BaseModel::TABLE.' WHERE id=?;', $id);
	}

}
