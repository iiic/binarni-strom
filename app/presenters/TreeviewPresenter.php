<?php

use \Nette\Application\UI\Form;
use \Nette\Utils\HTML;

class TreeviewPresenter extends BasePresenter
{

	private $all;



	function __construct(\Nette\DI\Container $context = NULL) {
		parent::__construct($context);
	}



	public function startup() {
		parent::startup();
	}



	public function renderDefault() {
		$parents = array();
		$children = array();
		$this->all = $this->getContext()->TreeviewModel->getAll();
		foreach ($this->all as $key=>$value) {
			if($value->parent_id == NULL) {
				$parents[$value->id] = $value;
			} else {
				$children[$value->id] = $value;
			}
		}
		$this->template->parents = $parents;
		$this->template->children = $children;
	}



	public function actionAdd($id = 0) { // id = parent id
		$form = $this->getComponent('addForm');
		$form->setDefaults(array('parent_id' => $id));
	}



	public function actionEdit($id) {
		$data = $this->getContext()->TreeviewModel->get($id);
		$form = $this->getComponent('editForm');
		if ($data) {
			if (empty($data->parent_id)) {
				$data->parent_id = 0;
				$form->setDefaults($data);
			}
		} else {
			$form->addError('This item does not exist.');
		}
	}



	protected function createComponentAddForm($name) {
		$items[0] = ' ';
		if(!$this->all) {
			$this->all = $this->getContext()->TreeviewModel->getAll();
		}
		foreach ($this->all as $key => $row) {
			$items[$row->id] = $row->name;
		}
		$form = new Form($this, $name);
		$renderer = $form->getRenderer();
		$renderer->wrappers['label']['suffix'] = ':';
		$form->addGroup('Add new item')
			->setOption('description', Html::el('p')
			->setHtml('[<a href="#x">x</a>]')
			);
		$form->addText('name', 'Name', 30)
			->addRule(Form::FILLED, 'You have to fill name.');
		$form->addSelect('parent_id', 'Parent', $items, 15);
		$form->addTextArea('notice', 'Notice', 40, 4)
			->addRule(Form::MAX_LENGTH, 'Notice must be at least %d characters.', 250);
		$form->addSubmit('add', 'Add');
		$form->onSuccess[] = array($this, 'addOnFormSubmitted');
	}



	protected function createComponentEditForm($name) {
		$items[0] = ' ';
		if(!$this->all) {
			$this->all = $this->getContext()->TreeviewModel->getAll();
		}
		foreach ($this->all as $key => $row) {
			$items[$row->id] = $row->name;
		}
		$form = new Form($this, $name);
		$renderer = $form->getRenderer();
		$renderer->wrappers['label']['suffix'] = ':';
		$form->addGroup('Edit item');
		$form->addText('name', 'Name', 30)
			->addRule(Form::FILLED, 'You have to fill name.');
		$form->addSelect('parent_id', 'Parent', $items, 15);
		$form->addTextArea('notice', 'Notice', 40, 4)
			->addRule(Form::MAX_LENGTH, 'Notice must be at least %d characters.', 250);
		$form->addSubmit('edit', 'Edit');
		$form->onSuccess[] = array($this, 'editOnFormSubmitted');
	}



	public function editOnFormSubmitted(Form $form) {
		try {
			$id = $this->getParam('id');
			$values = $form->getValues();
			if ($values['parent_id'] == 0) {
				$values['parent_id'] = NULL;
			}
			$this->getContext()->TreeviewModel->update($id, $values);
			$this->flashMessage('The item has been edited.', 'ok');
			$this->redirect('treeview:');
		} catch (Exception $e) {
			$form->addError('The item has not been edited.');
			throw $e;
		}
	}



	public function addOnFormSubmitted(Form $form) {
		try {
			$values = $form->getValues();
			if ($values['parent_id'] == 0) {
				$values['parent_id'] = NULL;
			}
			$this->getContext()->TreeviewModel->insert($values);
			$this->flashMessage('The item has been added.', 'ok');
			$this->redirect('treeview:');
		} catch (Exception $e) {
			$form->addError('The item has not been added.');
			throw $e;
		}
	}



	public function actionDelete($id) {
		$data = $this->getContext()->TreeviewModel->getName($id);
		if ($data) {
			$this->template->item = $data;
		} else {
			$this->flashMessage('This item does not exist.');
			$this->redirect('treeview:');
		}
	}



	public function handleFastDelete($id) {
		$data = $this->getContext()->TreeviewModel->getName($id);
		if ($data) {
			$id = $this->getParam('id');
			$this->getContext()->TreeviewModel->delete($id);
			$this->flashMessage('The item has been deleted.');
		} else {
			$this->flashMessage('This item does not exist.');
		}
		$this->redirect('treeview:');
	}



	protected function createComponentDelete($name) {
		$form = new Form($this, $name);
		$form->addSubmit('delete', 'Delete');
		$form->addSubmit('cancel', 'Cancel');
		$form->onSuccess[] = array($this, 'deleteOnFormSubmitted');
	}



	public function deleteOnFormSubmitted(Form $form) {
		if ($form['delete']->isSubmittedBy()) {
			try {
				$id = $this->getParam('id');
				$this->getContext()->TreeviewModel->delete($id);
				$this->flashMessage('The item has been deleted.');
				$this->redirect('treeview:');
			} catch (Exception $e) {
				$form->addError('The item has not been deleted.');
				throw $e;
			}
		} else {
			$this->redirect('treeview:');
		}
	}



	public function getChildren($children, $parent_id) {
		$return = FALSE;
		foreach ($children as $key=>$value) {
			if($value->parent_id == $parent_id){
				$return[$value->id] = $value;
			}
		}
		return $return;
	}

}
