-- Adminer 3.5.1 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `wiki`;
CREATE TABLE `wiki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `notice` varchar(250) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gui_acl_roles_ibfk_1` (`parent_id`),
  CONSTRAINT `wiki_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `wiki` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `wiki` (`id`, `parent_id`, `name`, `notice`) VALUES
(1,	NULL,	'savci',	'nae at sdk sdljt sdlskytjlsč'),
(2,	NULL,	'obojživelníci',	'žáby a tak'),
(3,	1,	'šelmy',	''),
(4,	3,	'kočkovití',	''),
(5,	NULL,	'plazi',	''),
(6,	5,	'želvy',	''),
(7,	NULL,	'ptáci',	''),
(8,	5,	'krokodýli',	''),
(9,	3,	'psovití',	'dlfskjs sdk sd skž č '),
(10,	4,	'malé kočky',	''),
(11,	9,	'vlk',	''),
(12,	10,	'rys ostrovid',	''),
(13,	10,	'gepard',	'salkč aslkj salkžjlka jslkdjfask jlsad '),
(14,	10,	'kočka domácí',	NULL),
(15,	9,	'liška polární',	NULL),
(16,	8,	'aligátorovití',	NULL),
(17,	7,	'běžci',	''),
(18,	7,	'letci',	''),
(19,	17,	'pštrosi',	''),
(20,	18,	'tučňáci',	NULL),
(21,	18,	'kukačky',	NULL);

-- 2013-08-20 00:28:41